# frozen_string_literal: true

module JH
  module AppearancesHelper
    extend ::Gitlab::Utils::Override

    override :default_brand_title
    def default_brand_title
      'GitLab Enterprise JH Edition'
    end
  end
end
