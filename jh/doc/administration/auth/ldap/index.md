---
type: reference
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 通用 LDAP 设置 **(FREE SELF)**

<!--
GitLab integrates with [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
to support user authentication.

This integration works with most LDAP-compliant directory servers, including:

- Microsoft Active Directory
  - [Microsoft Active Directory Trusts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc771568(v=ws.10)) are not supported.
- Apple Open Directory
- Open LDAP
- 389 Server

Users added through LDAP take a [licensed seat](../../../subscriptions/self_managed/index.md#billable-users).

GitLab Enterprise Editions (EE) include enhanced integration,
including group membership syncing and multiple LDAP server support.
-->

极狐GitLab 与 LDAP 集成，以支持用户身份验证。

此集成适用于大多数 LDAP 兼容目录服务器，包括：

- Microsoft Active Directory
  - 不支持 [Microsoft Active Directory Trusts](https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2008-R2-and-2008/cc771568(v=ws.10))。
- Apple Open Directory
- Open LDAP
- 389 Server

通过 LDAP 添加的用户获得[许可席位](../../../subscriptions/self_managed/index.md#billable-users)。

极狐GitLab 包括增强的集成功能，包括组成员同步和多 LDAP 服务器支持。

## 安全

<!--
GitLab assumes that LDAP users:

- Are not able to change their LDAP `mail`, `email`, or `userPrincipalName` attributes.
  An LDAP user allowed to change their email on the LDAP server can potentially
  [take over any account](#enabling-ldap-sign-in-for-existing-gitlab-users)
  on your GitLab server.
- Have unique email addresses. If not, it's possible for LDAP users with the same
  email address to share the same GitLab account.

We recommend against using LDAP integration if your LDAP users are
allowed to change their `mail`, `email` or `userPrincipalName` attributes on
the LDAP server, or share email addresses.
-->

假设 LDAP 用户：

* 无法更改他们的 `mail`、`email` 或 `userPrincipalName` 属性。允许在 LDAP 服务器上更改电子邮件的用户，可能会[接管您服务器上的任何账户](#enabling-ldap-sign-in-for-existing-gitlab-users)。
* 拥有唯一的电子邮件地址。否则，具有相同电子邮件地址的 LDAP 用户可能会共享相同的账户。

如果您的 LDAP 用户被允许在 LDAP 服务器上更改他们的 `mail`、`email` 或 `userPrincipalName` 属性，或者共享电子邮件地址，我们建议不要使用 LDAP 集成。

### 删除用户

<!--
Users deleted from the LDAP server are immediately blocked from signing in
to GitLab. However, there's an LDAP check cache time of one hour (which is
[configurable](#adjusting-ldap-user-sync-schedule) for GitLab Premium users).
This means users already signed-in or who are using Git over SSH can access
GitLab for up to one hour. Manually block the user in the GitLab Admin Area
to immediately block all access.
-->

从 LDAP 服务器中删除的用户，会立即被组织登录 LDAP。然而，LDAP 检查缓存时间为 1 小时（对于专业版用户是[可配置的](#adjusting-ldap-user-sync-schedule)）。
这意味着已经登录或通过 SSH 使用 git 的用户还可以访问最多一个小时，您可以在管理中心手动立即阻止所有访问。

## Git 密码认证

<!--
LDAP-enabled users can authenticate with Git using their GitLab username or
email and LDAP password, even if password authentication for Git is disabled
in the application settings.
-->

即使在应用设置中禁用了 Git 密码身份认证，启用 LDAP 的用户可以使用用户名、电子邮件和 LDAP 密码对 Git 进行身份认证。

## Enabling LDAP sign-in for existing GitLab users

<!--
When a user signs in to GitLab with LDAP for the first time and their LDAP
email address is the primary email address of an existing GitLab user, the
LDAP DN is associated with the existing user. If the LDAP email attribute
isn't found in the GitLab user database, a new user is created.

In other words, if an existing GitLab user wants to enable LDAP sign-in for
themselves, they should check that their GitLab email address matches their
LDAP email address, and then sign into GitLab by using their LDAP credentials.
-->

当用户第一次使用 LDAP 登录极狐GitLab，并且他们的 LDAP 电子邮件地址是现有用户的主要电子邮件时，LDAP DN 与现有用户相关联。如果在极狐 GitLab 用户数据库中找不到 LDAP 的电子邮件，则会创建一个新用户。

也就是说，如果现有用户想要为自己启用 LDAP 登录，应检查他们在极狐 GitLab 中的电子邮件地址与他们的 LDAP 电子邮件地址匹配，然后可以使用 LDAP 凭据登录极狐GitLab。 

<!--
## Google Secure LDAP

> Introduced in GitLab 11.9.

[Google Cloud Identity](https://cloud.google.com/identity/) provides a Secure
LDAP service that can be configured with GitLab for authentication and group sync.
See [Google Secure LDAP](google_secure_ldap.md) for detailed configuration instructions.
-->

## 配置

<!--
To enable LDAP integration you need to add your LDAP server settings in
`/etc/gitlab/gitlab.rb` or `/home/git/gitlab/config/gitlab.yml` for Omnibus
GitLab and installations from source respectively.

There is a Rake task to check LDAP configuration. After configuring LDAP
using the documentation below, see [LDAP check Rake task](../../raketasks/check.md#ldap-check)
for information on the LDAP check Rake task.

NOTE:
The `encryption` value `simple_tls` corresponds to 'Simple TLS' in the LDAP
library. `start_tls` corresponds to StartTLS, not to be confused with regular TLS.
Normally, if you specify `simple_tls` it is on port 636, while `start_tls` (StartTLS)
would be on port 389. `plain` also operates on port 389. Removed values: `tls` was replaced
with `start_tls` and `ssl` was replaced with `simple_tls`.

LDAP users must have a set email address, regardless of whether or not it's used
to sign in.
-->

要启用 LDAP 集成，当使用 Omnibus GitLab 时，您需要在 `/etc/gitlab/gitlab.rb` 中添加 LDAP 服务器设置；当使用源安装时，您需要在 `/home/git/gitlab/config/gitlab.yml` 中添加 LDAP 服务器设置。

有一个 Rake 任务可用于检查 LDAP 配置。使用下方文档配置 LDAP 后，请参阅 [LDAP 检查 Rake 任务](../../raketasks/check.md#ldap-check)获取更多信息。

NOTE:
`encryption` 的值 `simple_tls` 对应于 LDAP 库中的“Simple TLS”。`start_tls` 对应于 StartTLS，不要与常规 TLS 混淆。通常，如果您指定 `simple_tls` 在端口 636 上，而 `start_tls`（StartTLS）将在端口 389 上。`plain` 也在端口 389 上运行。删除的值：`tls` 被替换为 `start_tls`，`ssl` 被替换为 `simple_tls`。

LDAP 用户必须设置一个电子邮件地址，无论是否用于登录。

### 配置案例

**Omnibus 配置**

```ruby
gitlab_rails['ldap_enabled'] = true
gitlab_rails['prevent_ldap_sign_in'] = false
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'LDAP',
  'host' =>  'ldap.mydomain.com',
  'port' => 389,
  'uid' => 'sAMAccountName',
  'encryption' => 'simple_tls',
  'verify_certificates' => true,
  'bind_dn' => '_the_full_dn_of_the_user_you_will_bind_with',
  'password' => '_the_password_of_the_bind_user',
  'verify_certificates' => true,
  'tls_options' => {
    'ca_file' => '',
    'ssl_version' => '',
    'ciphers' => '',
    'cert' => '',
    'key' => ''
  },
  'timeout' => 10,
  'active_directory' => true,
  'allow_username_or_email_login' => false,
  'block_auto_created_users' => false,
  'base' => 'dc=example,dc=com',
  'user_filter' => '',
  'attributes' => {
    'username' => ['uid', 'userid', 'sAMAccountName'],
    'email' => ['mail', 'email', 'userPrincipalName'],
    'name' => 'cn',
    'first_name' => 'givenName',
    'last_name' => 'sn'
  },
  'lowercase_usernames' => false,

  # EE Only
  'group_base' => '',
  'admin_group' => '',
  'external_groups' => [],
  'sync_ssh_keys' => false
  }
}
```

**源安装配置**

```yaml
production:
  # snip...
  ldap:
    enabled: false
    prevent_ldap_sign_in: false
    servers:
      main:
        label: 'LDAP'
        ...
```

### 基本配置

<!--
| Setting            | Description | Required | Examples |
|--------------------|-------------|----------|----------|
| `label`            | A human-friendly name for your LDAP server. It is displayed on your sign-in page. | **{check-circle}** Yes | `'Paris'` or `'Acme, Ltd.'` |
| `host`             | IP address or domain name of your LDAP server. | **{check-circle}** Yes | `'ldap.mydomain.com'` |
| `port`             | The port to connect with on your LDAP server. Always an integer, not a string. | **{check-circle}** Yes | `389` or `636` (for SSL) |
| `uid`              | LDAP attribute for username. Should be the attribute, not the value that maps to the `uid`. | **{check-circle}** Yes | `'sAMAccountName'` or `'uid'` or `'userPrincipalName'` |
| `bind_dn`          | The full DN of the user you bind with. | **{dotted-circle}** No | `'america\momo'` or `'CN=Gitlab,OU=Users,DC=domain,DC=com'` |
| `password`         | The password of the bind user. | **{dotted-circle}** No | `'your_great_password'` |
| `encryption`       | Encryption method. The `method` key is deprecated in favor of `encryption`. | **{check-circle}** Yes | `'start_tls'` or `'simple_tls'` or `'plain'` |
| `verify_certificates` | Enables SSL certificate verification if encryption method is `start_tls` or `simple_tls`. Defaults to true. | **{dotted-circle}** No | boolean |
| `timeout`          | Set a timeout, in seconds, for LDAP queries. This helps avoid blocking a request if the LDAP server becomes unresponsive. A value of `0` means there is no timeout. (default: `10`) | **{dotted-circle}** No | `10` or `30` |
| `active_directory` | This setting specifies if LDAP server is Active Directory LDAP server. For non-AD servers it skips the AD specific queries. If your LDAP server is not AD, set this to false. | **{dotted-circle}** No | boolean |
| `allow_username_or_email_login` | If enabled, GitLab ignores everything after the first `@` in the LDAP username submitted by the user on sign-in. If you are using `uid: 'userPrincipalName'` on ActiveDirectory you need to disable this setting, because the userPrincipalName contains an `@`. | **{dotted-circle}** No | boolean |
| `block_auto_created_users` | To maintain tight control over the number of billable users on your GitLab installation, enable this setting to keep new users blocked until they have been cleared by an administrator (default: false). | **{dotted-circle}** No | boolean |
| `base` | Base where we can search for users. | **{check-circle}** Yes | `'ou=people,dc=gitlab,dc=example'` or `'DC=mydomain,DC=com'` |
| `user_filter`      | Filter LDAP users. Format: [RFC 4515](https://tools.ietf.org/search/rfc4515) Note: GitLab does not support `omniauth-ldap`'s custom filter syntax. | **{dotted-circle}** No | For examples, read [Examples of user filters](#examples-of-user-filters). |
| `lowercase_usernames` | If enabled, GitLab converts the name to lower case. | **{dotted-circle}** No | boolean |
-->

| 配置项           | 说明 | 是否必须配置 | 举例 |
|--------------------|-------------|----------|----------|
| `label`            | LDAP 服务器的人性化名称。在登录页面展示。 | **{check-circle}** Yes | `'Paris'` or `'Acme, Ltd.'` |
| `host`             | LDAP 服务器的 IP 地址或域名。 | **{check-circle}** Yes | `'ldap.mydomain.com'` |
| `port`             | 与 LDAP 服务器连接的端口。类型为整数而非字符串。 | **{check-circle}** Yes | `389` 或 `636` (用于 SSL) |
| `uid`              | 用户名的 LDAP 属性。应该是属性，而不是映射到 `uid` 的值。 | **{check-circle}** Yes | `'sAMAccountName'`、`'uid'` 或 `'userPrincipalName'` |
| `bind_dn`          | 绑定用户的完整 DN。 | **{dotted-circle}** No | `'america\momo'` 或 `'CN=Gitlab,OU=Users,DC=domain,DC=com'` |
| `password`         | 绑定用户的密码。 | **{dotted-circle}** No | `'your_great_password'` |
| `encryption`       | 加密方法。`method` 已废弃。 | **{check-circle}** Yes | `'start_tls'`、`'simple_tls'` 或 `'plain'` |
| `verify_certificates` | 如果加密方法为 `start_tls` 或 `simple_tls`，则启用 SSL 证书验证。默认为 true。 | **{dotted-circle}** No | boolean |
| `timeout`          | 为 LDAP 查询设置超时时间（以秒为单位）。当 LDAP 服务器无法响应时，有助于阻止请求。`0` 值表示没有超时（默认值：`10`） | **{dotted-circle}** No | `10` 或 `30` |
| `active_directory` | 该设置指定 LDAP 服务器是否为 Active Directory LDAP 服务器。对于非 AD 服务器，会跳过 AD 特定查询。如果您的 LDAP 服务器不是 AD，请将其设置为 false。 | **{dotted-circle}** No | boolean |
| `allow_username_or_email_login` | 如果启用，系统将忽略用户在登录时提交的 LDAP 用户名中第一个 `@` 之后的所有内容。如果您在 ActiveDirectory 上使用 `uid: 'userPrincipalName'`，您需要禁用这个设置，因为 userPrincipalName 包含一个 `@`。 | **{dotted-circle}** No | boolean |
| `block_auto_created_users` | 为了严格控制计费用户数量，请启用此设置阻止新用户直到他们被管理员清除（默认值：false）。 | **{dotted-circle}** No | boolean |
| `base` | 搜索用户的基础路径。 | **{check-circle}** Yes | `'ou=people,dc=gitlab,dc=example'` or `'DC=mydomain,DC=com'` |
| `user_filter`      | 过滤 LDAP 用户。格式：[RFC 4515](https://tools.ietf.org/search/rfc4515) Note: 不支持 `omniauth-ldap` 的自定义过滤器语法。 | **{dotted-circle}** No | 查阅[用户过滤器的案例](#examples-of-user-filters)。 |
| `lowercase_usernames` | 如果启用，系统转换用户名称为小写。 | **{dotted-circle}** No | boolean |

#### 用户过滤器样例

`user_filter` 字段语法的一些示例：

- `'(employeeType=developer)'`
- `'(&(objectclass=user)(|(samaccountname=momo)(samaccountname=toto)))'`

### SSL 配置

<!--
| Setting       | Description | Required | Examples |
|---------------|-------------|----------|----------|
| `ca_file`     | Specifies the path to a file containing a PEM-format CA certificate, for example, if you need to use an internal CA. | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | Specifies the SSL version for OpenSSL to use, if the OpenSSL default is not appropriate. | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | Specific SSL ciphers to use in communication with LDAP servers. | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | Client certificate. | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | Client private key. | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|---------------|-------------|----------|----------|
| `ca_file`     | 例如，当您需要使用内部 CA 时，指定 PEM 格式的 CA 证书文件的路径。 | **{dotted-circle}** No | `'/etc/ca.pem'` |
| `ssl_version` | 如果 OpenSSL 默认值不合适，指定 OpenSSL 的 SSL 版本。 | **{dotted-circle}** No | `'TLSv1_1'` |
| `ciphers`     | 用于与 LDAP 服务器通信的特定 SSL 密码。 | **{dotted-circle}** No | `'ALL:!EXPORT:!LOW:!aNULL:!eNULL:!SSLv2'` |
| `cert`        | 客户凭证。 | **{dotted-circle}** No | `'-----BEGIN CERTIFICATE----- <REDACTED> -----END CERTIFICATE -----'` |
| `key`         | 客户私钥。 | **{dotted-circle}** No | `'-----BEGIN PRIVATE KEY----- <REDACTED> -----END PRIVATE KEY -----'` |


### 属性配置

<!--
LDAP attributes that GitLab uses to create an account for the LDAP user. The specified
attribute can either be the attribute name as a string (for example, `'mail'`), or an
array of attribute names to try in order (for example, `['mail', 'email']`). Note that
the user's LDAP sign-in is the attribute specified as `uid` above.

| Setting      | Description | Required | Examples |
|--------------|-------------|----------|----------|
| `username`   | The username is used in paths for the user's own projects (like `gitlab.example.com/username/project`) and when mentioning them in issues, merge request and comments (like `@username`). If the attribute specified for `username` contains an email address, the GitLab username is part of the email address before the `@`. | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | LDAP attribute for user email. | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | LDAP attribute for user display name. If `name` is blank, the full name is taken from the `first_name` and `last_name`. | **{dotted-circle}** No | Attributes `'cn'`, or `'displayName'` commonly carry full names. Alternatively, you can force the use of `first_name` and `last_name` by specifying an absent attribute such as `'somethingNonExistent'`. |
| `first_name` | LDAP attribute for user first name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | LDAP attribute for user last name. Used when the attribute configured for `name` does not exist. | **{dotted-circle}** No | `'sn'` |
-->

系统为 LDAP 用户创建账户时，使用的 LDAP 属性。指定的属性可以是字符串形式的属性名称（例如，`'mail'`），也可以是要按顺序尝试的属性名称数组（例如，`['mail', 'email']`）。请注意，用户的 LDAP 登录时指定为 `uid` 的属性。

| 配置项       | 说明 | 是否必须配置 | 举例 |
|--------------|-------------|----------|----------|
| `username`   | 用户名用于用户自己项目的路径 (例如 `gitlab.example.com/username/project`) ，以及在议题、合并请求和评论中提及时 (例如 `@username`)。如果 `username` 包含电子邮件地址, 则地址中 `@` 之前的部分作为极狐GitLab 用户名。 | **{dotted-circle}** No | `['uid', 'userid', 'sAMAccountName']` |
| `email`      | 用户电子邮件的 LDAP 属性。 | **{dotted-circle}** No | `['mail', 'email', 'userPrincipalName']` |
| `name`       | 用户显示名称的 LDAP 属性。如果 `name` 为空, 完整的显示名称从 `first_name` 和 `last_name` 获取。 | **{dotted-circle}** No |  `'cn'` 或 `'displayName'` 属性通常带有全名。或者，您可以通过指定不存在的属性，例如  `'somethingNonExistent'`，来强制使用 `first_name` 和 `last_name`。|
| `first_name` | 用户 first name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'givenName'` |
| `last_name`  | 用户 last name 的 LDAP 属性。当 `name` 属性不存在时使用。 | **{dotted-circle}** No | `'sn'` |


### LDAP 同步配置 **(PREMIUM SELF)**

<!--
| Setting           | Description | Required | Examples |
|-------------------|-------------|----------|----------|
| `group_base`      | Base used to search for groups. | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | The CN of a group containing GitLab administrators. Note: Not `cn=administrators` or the full DN. | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | An array of CNs of groups containing users that should be considered external. Note: Not `cn=interns` or the full DN. | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | The LDAP attribute containing a user's public SSH key. | **{dotted-circle}** No | `'sshPublicKey'` or false if not set |
-->

| 配置项       | 说明 | 是否必须配置 | 举例 |
|-------------------|-------------|----------|----------|
| `group_base`      | 搜索用户组的基础路径。 | **{dotted-circle}** No | `'ou=groups,dc=gitlab,dc=example'` |
| `admin_group`     | 包含 GitLab 管理员的组的 CN。注意：不是`cn=administrators` 或完整的DN。 | **{dotted-circle}** No | `'administrators'` |
| `external_groups` | 包含应被视为外部用户的组 CN 数组。 注意：不是 `cn=interns` 或完整的 DN。 | **{dotted-circle}** No | `['interns', 'contractors']` |
| `sync_ssh_keys`   | 包含用户的公共 SSH 密钥的 LDAP 属性。 | **{dotted-circle}** No | `'sshPublicKey'`  或者 false（如果未设置）。 |

### 设置 LDAP 用户过滤器

<!--
If you want to limit all GitLab access to a subset of the LDAP users on your
LDAP server, the first step should be to narrow the configured `base`. However,
it's sometimes necessary to further filter users. In this case, you can set
up an LDAP user filter. The filter must comply with
[RFC 4515](https://tools.ietf.org/search/rfc4515).
-->

如果您想要 LDAP 服务器上的一部分 LDAP 用户具有访问极狐GitLab 的权限，第一步应该缩小配置的 `base` 范围。但是，有时必须进一步过滤用户。在这种情况下，您可以设置 LDAP 用户过滤器。过滤器必须符合 [RFC 4515](https://tools.ietf.org/search/rfc4515)。

**Omnibus 配置**

```ruby
gitlab_rails['ldap_servers'] = {
'main' => {
  # snip...
  'user_filter' => '(employeeType=developer)'
  }
}
```

**源安装配置**

```yaml
production:
  ldap:
    servers:
      main:
        # snip...
        user_filter: '(employeeType=developer)'
```
<!--
If you want to limit access to the nested members of an Active Directory
group, use the following syntax:

```plaintext
(memberOf:1.2.840.113556.1.4.1941:=CN=My Group,DC=Example,DC=com)
```

For more information about this "LDAP_MATCHING_RULE_IN_CHAIN" filter, see the following
[Microsoft Search Filter Syntax](https://docs.microsoft.com/en-us/windows/win32/adsi/search-filter-syntax) document.
Support for nested members in the user filter shouldn't be confused with
[group sync nested groups support](#supported-ldap-group-typesattributes). **(PREMIUM SELF)**

GitLab does not support the custom filter syntax used by OmniAuth LDAP.
-->

如果要限制对 Active Directory 组的嵌套成员的访问，请使用以下语法：
```plaintext
(memberOf:1.2.840.113556.1.4.1941:=CN=My Group,DC=Example,DC=com)
```
获取 "LDAP_MATCHING_RULE_IN_CHAIN" 过滤器的更多信息，请参阅 [Microsoft 搜索过滤器语法](https://docs.microsoft.com/en-us/windows/win32/adsi/search-filter-syntax) 文档。用户过滤器中对嵌套成员的支持不应与[组同步嵌套组支持](#supported-ldap-group-typesattributes) 混淆。**(PREMIUM SELF)**

不支持 OmniAuth LDAP 使用的自定义过滤器语法。

#### 转义特殊字符

<!--
The `user_filter` DN can contain special characters. For example:

- A comma:

  ```plaintext
  OU=GitLab, Inc,DC=gitlab,DC=com
  ```

- Open and close brackets:

  ```plaintext
  OU=Gitlab (Inc),DC=gitlab,DC=com
  ```

  These characters must be escaped as documented in
  [RFC 4515](https://tools.ietf.org/search/rfc4515).

- Escape commas with `\2C`. For example:

  ```plaintext
  OU=GitLab\2C Inc,DC=gitlab,DC=com
  ```

- Escape open and close brackets with `\28` and `\29`, respectively. For example:

  ```plaintext
  OU=Gitlab \28Inc\29,DC=gitlab,DC=com
  ```
-->

`user_filter` DN 可以包含特殊字符。 例如：

- 逗号：

  ```plaintext
  OU=GitLab, Inc,DC=gitlab,DC=com
  ```

- 括号：

  ```plaintext
  OU=Gitlab (Inc),DC=gitlab,DC=com
  ```


  这些字符必须按照 [RFC 4515](https://tools.ietf.org/search/rfc4515) 文档所述进行转义。

- 使用 `\2C` 转义逗号。例如：

  ```plaintext
  OU=GitLab\2C Inc,DC=gitlab,DC=com
  ```

- 同样地，使用 `\28` 和 `\29` 转义括号。例如：

  ```plaintext
  OU=Gitlab \28Inc\29,DC=gitlab,DC=com
  ```


### 启用 LDAP 用户名小写
<!--
Some LDAP servers, depending on their configurations, can return uppercase usernames.
This can lead to several confusing issues such as creating links or namespaces with uppercase names.

GitLab can automatically lowercase usernames provided by the LDAP server by enabling
the configuration option `lowercase_usernames`. By default, this configuration option is `false`.
-->
某些 LDAP 服务器，根据其配置可以返回大写的用户名。这可能导致一些令人困惑的问题，比如使用大写名称创建链接或命名空间。

极狐GitLab 可以通过启用 `lowercase_usernames` 配置项，自动将 LDAP 服务器提供的用户名小写。默认情况下，该配置选项为 `false`。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`:

   ```ruby
   gitlab_rails['ldap_servers'] = {
   'main' => {
     # snip...
     'lowercase_usernames' => true
     }
   }
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**Source configuration**

1. 编辑 `config/gitlab.yaml`:

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           lowercase_usernames: true
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

### 禁用 LDAP web 登录
<!--
It can be useful to prevent using LDAP credentials through the web UI when
an alternative such as SAML is preferred. This allows LDAP to be used for group
sync, while also allowing your SAML identity provider to handle additional
checks like custom 2FA.

When LDAP web sign in is disabled, users don't see an **LDAP** tab on the sign-in page.
This does not disable [using LDAP credentials for Git access](#git-password-authentication).
-->
当首选 SAML 等替代方法时，阻止通过 Web UI 使用 LDAP 凭据会很有用。 这允许 LDAP 用于组同步，同时还允许您的 SAML 身份提供商处理额外的检查，如自定义 2FA。

禁用 LDAP Web 登录后，用户在登录页面上看不到 **LDAP** 选项。这不会禁用[使用 LDAP 凭据进行 Git 访问](#git-password-authentication)。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['prevent_ldap_sign_in'] = true
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使配置生效。

**源安装配置**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   production:
     ldap:
       prevent_ldap_sign_in: true
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使配置生效。

### 使用加密凭证
<!--
Instead of having the LDAP integration credentials stored in plaintext in the configuration files, you can optionally
use an encrypted file for the LDAP credentials. To use this feature, you first need to enable
[GitLab encrypted configuration](../../encrypted_configuration.md).

The encrypted configuration for LDAP exists in an encrypted YAML file. By default the file will be created at
`shared/encrypted_configuration/ldap.yaml.enc`. This location is configurable in the GitLab configuration.

The unencrypted contents of the file should be a subset of the secret settings from your `servers` block in the LDAP
configuration.

The supported configuration items for the encrypted file are:

- `bind_dn`
- `password`

The encrypted contents can be configured with the [LDAP secret edit Rake command](../../raketasks/ldap.md#edit-secret).
-->

除了将 LDAP 集成凭证以明文形式存储在配置文件中，您可以选择为 LDAP 凭证使用加密文件。要使用该功能，首先需要启用 [GitLab 加密配置](../../encrypted_configuration.md)。

LDAP 的加密配置存放于加密的 YAML 文件中。默认情况下，该文件将在 `shared/encrypted_configuration/ldap.yaml.enc` 中创建，位置可以配置。

文件中的未加密内容应该是 LDAP 配置中  `servers` 的密钥配置。

加密文件的配置项有：

- `bind_dn`
- `password`

可以使用 [LDAP 密钥编辑 Rake 命令](../../raketasks/ldap.md#edit-secret)配置加密内容。

**Omnibus 配置**

如果最初 LDAP 配置如下：

1. 在 `/etc/gitlab/gitlab.rb` 中：

  ```ruby
    gitlab_rails['ldap_servers'] = {
    'main' => {
      # snip...
      'bind_dn' => 'admin',
      'password' => '123'
      }
    }
  ```

1. 编辑加密密钥：

   ```shell
   sudo gitlab-rake gitlab:ldap:secret:edit EDITOR=vim
   ```

1. LDAP 密钥的未加密内容应按以下案例配置：

   ```yaml
   main:
     bind_dn: admin
     password: '123'
   ```

1. 编辑 `/etc/gitlab/gitlab.rb` 并移除 `user_bn` 和 `password` 设置。

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**源安装配置**

如果最初 LDAP 配置如下：

1. 在 `config/gitlab.yaml` 中：

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           bind_dn: admin
           password: '123'
   ```

1. 编辑加密密钥：

   ```shell
   bundle exec rake gitlab:ldap:secret:edit EDITOR=vim RAILS_ENVIRONMENT=production
   ```

1. LDAP 密钥的未加密内容应按以下案例配置：

   ```yaml
   main:
    bind_dn: admin
    password: '123'
   ```

1. 编辑 `config/gitlab.yaml` 并移除 `user_bn` 和 `password` 设置。

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

## 加密

### TLS 服务器认证
<!--
There are two encryption methods, `simple_tls` and `start_tls`.

For either encryption method, if setting `verify_certificates: false`, TLS
encryption is established with the LDAP server before any LDAP-protocol data is
exchanged but no validation of the LDAP server's SSL certificate is performed.
-->

支持两种加密方法，`simple_tls` 和 `start_tls`。

对于任一种加密方法，如果设置了 `verify_certificates: false`，在交换 LDAP 协议数据之前，先与 LDAP 服务器建立加密连接但不验证 LDAP 服务器的 SSL 证书。

### 限制

#### TLS 客户端认证
<!--
Not implemented by `Net::LDAP`.

You should disable anonymous LDAP authentication and enable simple or SASL
authentication. The TLS client authentication setting in your LDAP server cannot
be mandatory and clients cannot be authenticated with the TLS protocol.
-->

未由 `Net::LDAP` 实现。

您应该禁用匿名 LDAP 身份验证，并启用简单或 SASL 身份验证。LDAP 服务器中的 TLS 客户端身份验证不是强制性的，并且客户端无法使用 TLS 协议进行身份验证。

## 多 LDAP 服务器 **(PREMIUM SELF)**

<!--
With GitLab, you can configure multiple LDAP servers that your GitLab instance
connects to.

To add another LDAP server:

1. Duplicate the settings under [the main configuration](#configuration).
1. Edit them to match the additional LDAP server.

Be sure to choose a different provider ID made of letters a-z and numbers 0-9.
This ID is stored in the database so that GitLab can remember which LDAP
server a user belongs to.

![Multiple LDAP Servers Sign in](img/multi_login.gif)

Based on the example illustrated on the image above,
our `gitlab.rb` configuration would look like:

```ruby
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'GitLab AD',
  'host' =>  'ad.example.org',
  'port' => 636,
  ...
  },

'secondary' => {
  'label' => 'GitLab Secondary AD',
  'host' =>  'ad-secondary.example.net',
  'port' => 636,
  ...
  },

'tertiary' => {
  'label' => 'GitLab Tertiary AD',
  'host' =>  'ad-tertiary.example.net',
  'port' => 636,
  ...
  }

}
```

If you configure multiple LDAP servers, use a unique naming convention for the
`label` section of each entry. That label is used as the display name of the tab
shown on the sign-in page.
-->

您可以为您的实例配置连接多个 LDAP 服务器。

要添加另一个 LDAP 服务器：

1. 复制 [主配置](#configuration) 下的设置。
2. 编辑复制的设置以匹配额外的 LDAP 服务器。

务必选择一个不同的提供商 ID，ID 可由字母 a-z 和数字 0-9 组成。此 ID 存储在数据库中，以便实例可以记住用户属于哪个 LDAP 服务器。

![Multiple LDAP Servers Sign in](img/multi_login.gif)

基于上图的示例，`gitlab.rb` 应按下方案例进行配置：

```ruby
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'GitLab AD',
  'host' =>  'ad.example.org',
  'port' => 636,
  ...
  },

'secondary' => {
  'label' => 'GitLab Secondary AD',
  'host' =>  'ad-secondary.example.net',
  'port' => 636,
  ...
  },

'tertiary' => {
  'label' => 'GitLab Tertiary AD',
  'host' =>  'ad-tertiary.example.net',
  'port' => 636,
  ...
  }

}
```

如果您配置多个 LDAP 服务器，请为每个条目的 `label` 使用唯一的命名。该标签作为登录页面上选项卡的显示名称。

## 用户同步 **(PREMIUM SELF)**
<!--
Once per day, GitLab runs a worker to check and update GitLab
users against LDAP.

The process executes the following access checks:

- Ensure the user is still present in LDAP.
- If the LDAP server is Active Directory, ensure the user is active (not
  blocked/disabled state). This is checked only if
  `active_directory: true` is set in the LDAP configuration.

In Active Directory, a user is marked as disabled/blocked if the user
account control attribute (`userAccountControl:1.2.840.113556.1.4.803`)
has bit 2 set.
-->

系统将运行一个 worker 用来检查和更新 LDAP 用户，每天一次。

该进程执行以下访问检查：

* 确保用户仍存在于 LDAP 中。
* 如果 LDAP 服务器是 Active Directory，请确保用户处于 active 状态（不能时 blocked 或 disabled 状态）。只有在 LDAP 设置了 `active_directory: true` 时，才会执行检查。

<!-- vale gitlab.Spelling = NO -->
<!--
For more information, see [Bitmask Searches in LDAP](https://ctovswild.com/2009/09/03/bitmask-searches-in-ldap/).
-->

获取更多信息，查阅 [LDAP 的 Bitmask 搜索](https://ctovswild.com/2009/09/03/bitmask-searches-in-ldap/)。

<!-- vale gitlab.Spelling = YES -->
<!--
The user is set to an `ldap_blocked` state in GitLab if the previous conditions
fail. This means the user cannot sign in or push or pull code.

The process also updates the following user information:

- Email address
- SSH public keys (if `sync_ssh_keys` is set)
- Kerberos identity (if Kerberos is enabled)

The LDAP sync process:

- Updates existing users.
- Creates new users on first sign in.
-->

若检查失败，用户将被设置为 `ldap_blocked` 状态，这意味着用户无法登录、推送或拉取代码。

该过程还会更新以下用户信息：

- 电子邮件地址
- SSH 公钥（如果设置了 `sync_ssh_keys`）
- Kerberos 身份（如果启用了 Kerberos）

LDAP 同步过程：

- 更新现有用户。
- 首次登录时创建新用户。

### 调整 LDAP 用户同步计划 **(PREMIUM SELF)**
<!--
By default, GitLab runs a worker once per day at 01:30 a.m. server time to
check and update GitLab users against LDAP.

You can manually configure LDAP user sync times by setting the
following configuration values, in cron format. If needed, you can
use a [crontab generator](http://www.crontabgenerator.com).
The example below shows how to set LDAP user
sync to run once every 12 hours at the top of the hour.
-->

默认情况下，每天在服务器时间上午 01:30 运行一次 worker，检查和更新用户。

您可以使用 cron 格式，手动配置 LDAP 用户同步时间。如果需要，您可以使用 [crontab 生成器](http://www.crontabgenerator.com)。下面的示例展示了如何将 LDAP 用户同步设置为每 12 小时运行一次。

**Omnibus 安装方式**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_sync_worker_cron'] = "0 */12 * * *"
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**源安装方式**

1. 编辑 `config/gitlab.yaml`:

   ```yaml
   cron_jobs:
     ldap_sync_worker_cron:
       "0 */12 * * *"
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

## 群组同步 **(PREMIUM SELF)**

<!--
If your LDAP supports the `memberof` property, when the user signs in for the
first time GitLab triggers a sync for groups the user should be a member of.
That way they don't need to wait for the hourly sync to be granted
access to their groups and projects.

A group sync process runs every hour on the hour, and `group_base` must be set
in LDAP configuration for LDAP synchronizations based on group CN to work. This allows
GitLab group membership to be automatically updated based on LDAP group members.

The `group_base` configuration should be a base LDAP 'container', such as an
'organization' or 'organizational unit', that contains LDAP groups that should
be available to GitLab. For example, `group_base` could be
`ou=groups,dc=example,dc=com`. In the configuration file it looks like the
following.
-->

如果您的 LDAP 支持 `memberof` 属性，当用户第一次登录极狐GitLab 时，将触发用户所在组的同步。这样他们不需要等待经过每小时的同步后，才能访问他们的小组和项目。

组同步进程每小时执行一次，必须在 LDAP 配置中设置 `group_base`，使 LDAP 同步基于组的 CN。极狐GitLab 的群组成员将基于 LDAP 的组成员自动更新。

`group_base` 配置应该是一个基本的 LDAP `container`，例如一个 'organization' 或 'organizational unit'，包含极狐GitLab 可用的 LDAP 组。例如，`group_base` 可以配置为 `ou=groups,dc=example,dc=com`，在配置文件中如下例。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
   'main' => {
     # snip...
     'group_base' => 'ou=groups,dc=example,dc=com',
     }
   }
   ```

1. [应用您的更改](../../restart_gitlab.md#omnibus-gitlab-reconfigure)。

**源安装配置**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           group_base: ou=groups,dc=example,dc=com
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

<!--
To take advantage of group sync, group owners or maintainers need to [create one
or more LDAP group links](#adding-group-links).
-->

为实现群组同步，群组的拥有者或维护需要[创建一个或多个 LDAP 群组链接](#adding-group-links)。

### 添加群组链接 **(PREMIUM SELF)**

<!--
For information on adding group links by using CNs and filters, refer to the
[GitLab groups documentation](../../../user/group/index.md#manage-group-memberships-via-ldap).
-->

获取更多关于使用 CN 和 过滤器添加群组链接的信息，参考[群组文档](../../../user/group/index.md#manage-group-memberships-via-ldap)。

### 管理员同步 **(PREMIUM SELF)**
<!--
As an extension of group sync, you can automatically manage your global GitLab
administrators. Specify a group CN for `admin_group` and all members of the
LDAP group will be given administrator privileges. The configuration looks
like the following.

NOTE:
Administrators are not synced unless `group_base` is also
specified alongside `admin_group`. Also, only specify the CN of the `admin_group`,
as opposed to the full DN.
-->

作为组同步的扩展，您可以自动管理您的全局管理员。通过 `admin_group` 指定组的 CN，所有在这个 LDAP 组内的成员都会获得管理员去权限。配置举例如下所示。

NOTE:
管理员不会被同步，除非 `group_base` 也与 `admin_group` 一起指定。同样，只需要指定 `admin_group` 的 CN，而不是完整的 DN。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
   'main' => {
     # snip...
     'group_base' => 'ou=groups,dc=example,dc=com',
     'admin_group' => 'my_admin_group',
     }
   }
   ```

1. [应用您的更改](../../restart_gitlab.md#omnibus-gitlab-reconfigure)。

**源安装配置**

1. 编辑 `/home/git/gitlab/config/gitlab.yml`：

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           group_base: ou=groups,dc=example,dc=com
           admin_group: my_admin_group
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使变更生效。

### 全局群组成员锁 **(PREMIUM SELF)**
<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/1793) in GitLab 12.0.

"Lock memberships to LDAP synchronization" setting allows instance administrators
to lock down user abilities to invite new members to a group.

When enabled, the following applies:

- Only administrator can manage memberships of any group including access levels.
- Users are not allowed to share project with other groups or invite members to
  a project created in a group.

To enable it you need to:

1. [Enable LDAP](#configuration)
1. Go to the Admin Area (**{admin}**) and select **Settings > Visibility and access controls**.
1. Ensure the **Lock memberships to LDAP synchronization** checkbox is selected.
-->

"LDAP 同步时锁定成员身份"设置允许实例管理员锁定用户邀请新成员加入群组的权限。

当启用时，应用以下功能：

* 只有管理员可以管理任何群组的成员资格，包括访问级别。
* 不许云用户与其它群组共享项目，或邀请成员加入本群组的项目。

### 调整 LDAP 群组同步计划 **(PREMIUM SELF)**
<!--
By default, GitLab runs a group sync process every hour, on the hour.
The values shown are in cron format. If needed, you can use a
[Crontab Generator](http://www.crontabgenerator.com).

WARNING:
Do not start the sync process too frequently as this
could lead to multiple syncs running concurrently. This is primarily a concern
for installations with a large number of LDAP users. Review the
[LDAP group sync benchmark metrics](#benchmarks) to see how
your installation compares before proceeding.

You can manually configure LDAP group sync times by setting the
following configuration values. The example below shows how to set group
sync to run once every two hours at the top of the hour.
-->

默认情况下，系统每小时运行一次群组同步。显示值为 cron 格式。如果需要，您可以使用 [Crontab 生成器](http://www.crontabgenerator.com)。

WARNING:
不要频繁启动同步流程，因为这可能导致多个同步同时运行。尤其对于具有大量 LDAP 用户的实例造成问题。在继续之前，查看 [LDAP 组同步基准测试指标](#benchmarks)，与您的实例进行比较。

您可以通过设置以下配置值，手动配置 LDAP 组同步时间。下面的示例展示了如何将群组同步设置为每两个小时在整点运行一次。

**Omnibus 安装**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_group_sync_worker_cron'] = "0 */2 * * * *"
   ```

1. [重新配置极狐GitLab](../../restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

**源安装**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   cron_jobs:
     ldap_group_sync_worker_cron:
         "*/30 * * * *"
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

### 外部群组 **(PREMIUM SELF)**
<!--
Using the `external_groups` setting will allow you to mark all users belonging
to these groups as [external users](../../../user/permissions.md#external-users).
Group membership is checked periodically through the `LdapGroupSync` background
task.
-->

使用 `external_groups` 设置，允许您将属于这些组的所有用户标记为 [外部用户](../../../user/permissions.md#external-users)。通过 `LdapGroupSync` 后台任务定期检查群组成员身份。

**Omnibus 配置**

1. 编辑 `/etc/gitlab/gitlab.rb`：

   ```ruby
   gitlab_rails['ldap_servers'] = {
   'main' => {
     # snip...
     'external_groups' => ['interns', 'contractors'],
     }
   }
   ```

1. [应用您的变更](../../restart_gitlab.md#omnibus-gitlab-reconfigure)。

**源安装配置**

1. 编辑 `config/gitlab.yaml`：

   ```yaml
   production:
     ldap:
       servers:
         main:
           # snip...
           external_groups: ['interns', 'contractors']
   ```

1. [重启极狐GitLab](../../restart_gitlab.md#installations-from-source) 使更改生效。

### 群组同步技术细节
<!--
This section outlines what LDAP queries are executed and what behavior you
can expect from group sync.

Group member access are downgraded from a higher level if their LDAP group
membership changes. For example, if a user the Owner role in a group and the
next group sync reveals they should only have the Developer role, their
access is adjusted accordingly. The only exception is if the user is the
last owner in a group. Groups need at least one owner to fulfill
administrative duties.
-->

本节概述了 LDAP 查询是如何执行的以及群组同步的表现。

如果 LDAP 组成员身份发生变化，群组成员的权限将从更高的级别降级。例如，如果用户在组中的

#### 支持的 LDAP 组属性
<!--
GitLab supports LDAP groups that use member attributes:

- `member`
- `submember`
- `uniquemember`
- `memberof`
- `memberuid`

This means group sync supports (at least) LDAP groups with the following object
classes:

- `groupOfNames`
- `posixGroup`
- `groupOfUniqueNames`

Other object classes should work if members are defined as one of the
mentioned attributes.

Active Directory supports nested groups. Group sync recursively resolves
membership if `active_directory: true` is set in the configuration file.
-->

支持使用的成员属性：

- `member`
- `submember`
- `uniquemember`
- `memberof`
- `memberuid`

这以为这组同步支持需要 LDAP 组至少具有以下对象类：

- `groupOfNames`
- `posixGroup`
- `groupOfUniqueNames`

如果成员由以上提及的属性之一定义，则其它对象类应该有效。

Active Directory 支持嵌套组。 如果在配置文件中设置了 `active_directory: true`，则组同步会递归地解析成员身份。

##### Nested group memberships

<!--
Nested group memberships are resolved only if the nested group
is found in the configured `group_base`. For example, if GitLab sees a
nested group with DN `cn=nested_group,ou=special_groups,dc=example,dc=com` but
the configured `group_base` is `ou=groups,dc=example,dc=com`, `cn=nested_group`
is ignored.
-->

只有在配置的 `group_base` 中找到嵌套组时，才会解析嵌套组成员身份。 只有在配置的 `group_base` 中找到嵌套组时，才会解析嵌套组成员身份。例如，如果系统看到一个
DN 为 `cn=nested_group,ou=special_groups,dc=example,dc=com` 的嵌套组，但配置的 `group_base` 为 `ou=groups,dc=example,dc=com`，忽略 `cn=nested_group`。

#### 查询

<!--
- Each LDAP group is queried a maximum of one time with base `group_base` and
  filter `(cn=<cn_from_group_link>)`.
- If the LDAP group has the `memberuid` attribute, GitLab executes another
  LDAP query per member to obtain each user's full DN. These queries are
  executed with base `base`, scope 'base object', and a filter depending on
  whether `user_filter` is set. Filter may be `(uid=<uid_from_group>)` or a
  joining of `user_filter`.
-->

* 每个 LDAP 组使用基础 `group_base` 和过滤器 `(cn=<cn_from_group_link>)` 最多查询一次。

* 如果 LDAP 组具有 `memberuid` 属性，系统会为每个成员执行另一个 LDAP 查询以获取每个用户的完整 DN。这些查询使用基础`base`、范围 `base object` 和过滤器执行，具体取决于是否设置了 `user_filter`。 过滤器可以是 `(uid=<uid_from_group>)` 或`user_filter` 的集合。

#### 基准

<!--
Group sync was written to be as performant as possible. Data is cached, database
queries are optimized, and LDAP queries are minimized. The last benchmark run
revealed the following metrics:

For 20,000 LDAP users, 11,000 LDAP groups, and 1,000 GitLab groups with 10
LDAP group links each:

- Initial sync (no existing members assigned in GitLab) took 1.8 hours
- Subsequent syncs (checking membership, no writes) took 15 minutes

These metrics are meant to provide a baseline and performance may vary based on
any number of factors. This was an extreme benchmark and most instances don't
have near this many users or groups. Disk speed, database performance,
network and LDAP server response time affects these metrics.
-->

组同步的写入尽可能地提高性能。数据会被缓存，数据库已优化，并且最小化 LDAP 查询。最后一次基准测试实现了以下指标：

对于 20000 个 LDAP 用户、11000 个 LDAP 组合和 1000 个极狐GitLab 群组，每个群组有 10 个 LDAP 组链接：

* 初始同步（系统中未分配现有成员）耗时 1.8 小时
* 后续同步（检查成员资格，无写入）耗时 15 分钟

这些指标提供基准，性能可能会受多重因素影响。这是一个个极端测试，大多数实例不会拥有这么多用户或群组。磁盘速度、数据库性能、网络和 LDAP 服务器响应时间均会影响指标。

## 故障排查

查看 [LDAP 故障排查管理员指南](ldap-troubleshooting.md).
