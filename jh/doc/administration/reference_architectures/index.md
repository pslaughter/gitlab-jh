---
type: reference, concepts
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 参考架构 **(FREE SELF)**

<!--You can set up GitLab on a single server or scale it up to serve many users.
This page details the recommended Reference Architectures that were built and
verified by the GitLab Quality and Support teams.

Below is a chart representing each architecture tier and the number of users
they can handle. As your number of users grow with time, it's recommended that
you scale GitLab accordingly.-->

您可以在单个服务器上部署极狐GitLab，或者扩容以服务大量用户。本页面详细介绍已经构建认证过的推荐参考架构。

下图展示每个架构级别和它们分别能承载的用户数量。当您的用户数量随时间增长，推荐您据此扩容极狐GitLab。

![参考架构](img/reference-architectures.png)
<!-- Internal link: https://docs.google.com/spreadsheets/d/1obYP4fLKkVVDOljaI3-ozhmCiPtEeMblbBKkf2OADKs/edit#gid=1403207183 -->

<!--
Testing on these reference architectures was performed with the
[GitLab Performance Tool](https://gitlab.com/gitlab-org/quality/performance)
at specific coded workloads, and the throughputs used for testing were
calculated based on sample customer data. Select the
[reference architecture](#available-reference-architectures) that matches your scale.

Each endpoint type is tested with the following number of requests per second (RPS)
per 1,000 users:

- API: 20 RPS
- Web: 2 RPS
- Git (Pull): 2 RPS
- Git (Push): 0.4 RPS (rounded to nearest integer)

For GitLab instances with less than 2,000 users, it's recommended that you use
the [default setup](#automated-backups) by
[installing GitLab](../../install/index.md) on a single machine to minimize
maintenance and resource costs.

If your organization has more than 2,000 users, the recommendation is to scale the
GitLab components to multiple machine nodes. The machine nodes are grouped by
components. The addition of these nodes increases the performance and
scalability of to your GitLab instance.

When scaling GitLab, there are several factors to consider:

- Multiple application nodes to handle frontend traffic.
- A load balancer is added in front to distribute traffic across the application nodes.
- The application nodes connects to a shared file server and PostgreSQL and Redis services on the backend.

NOTE:
Depending on your workflow, the following recommended reference architectures
may need to be adapted accordingly. Your workload is influenced by factors
including how active your users are, how much automation you use, mirroring,
and repository/change size. Additionally the displayed memory values are
provided by [GCP machine types](https://cloud.google.com/compute/docs/machine-types).
For different cloud vendors, attempt to select options that best match the
provided architecture.
-->

在特定编码的工作负载中，已通过 GitLab 性能工具对参考架构进行了测试，用于测试的吞吐量根据样本客户数据计算。根据您的生产规模选择[参考架构](#可用的参考架构)。

每个端点类型都使用下方的每千名用户每秒请求数进行测试。

- API: 20 RPS
- Web: 2 RPS
- Git (拉取): 2 RPS
- Git (推送): 0.4 RPS (四舍五入到最接近的整数)

对于用户数低于 2000 的实例，推荐您使用在单个机器上[安装](../../install/index.md)时的[默认设置](#automated-backups)，以最小化维护和资源成本。

如果您的组织有超过 2000 个用户，建议将组件扩容到多个机器节点。机器节点按组件分组。添加这些节点可以提高实例的性能和可扩展性。

在扩容时，需要考虑以下几个因素：

* 多个应用节点处理前段流量。
* 在前方添加负载均衡器，以在应用节点之间分配流量。
* 应用节点连接到后端的共享文件服务器、PostgreSQL 服务和 Redis 服务。

NOTE:
根据您的工作流程，您可能需要相应调整以下推荐的参考架构。您的工作负载受多种因素影响，包括您的用户活跃程度、自动化程度、镜像以及存储库/更改大小。此外，显示的内存值由 [GCP 机器类型](https://cloud.google.com/compute/docs/machine-types)提供。对于不同的云服务提供商，尝试选择最能匹配架构的选项。

## 可用的参考架构

<!--
The following reference architectures are available:

- [Up to 1,000 users](1k_users.md)
- [Up to 2,000 users](2k_users.md)
- [Up to 3,000 users](3k_users.md)
- [Up to 5,000 users](5k_users.md)
- [Up to 10,000 users](10k_users.md)
- [Up to 25,000 users](25k_users.md)
- [Up to 50,000 users](50k_users.md)

A GitLab [Premium or Ultimate](https://about.gitlab.com/pricing/#self-managed) license is required
to get assistance from Support with troubleshooting the [2,000 users](2k_users.md)
and higher reference architectures.
[Read more about our definition of scaled architectures](https://about.gitlab.com/support/#definition-of-scaled-architecture).
-->

以下推荐架构可用：

- [1000 用户](1k_users.md)
- [2000 用户](2k_users.md)
- [3000 用户](3k_users.md)
- [5000 用户](5k_users.md)
- [10000 用户](10k_users.md)
- [25000 用户](25k_users.md)
- [50000 用户](50k_users.md)

在 2000 用户和更高用户数的参考架构中，如果需要技术支持协助进行故障排查，需要极狐GitLab 的[专业版或旗舰版](https://about.gitlab.cn/pricing/#self-managed)的许可证。

## 组件可用性

<!--
GitLab comes with the following components for your use, listed from least to
most complex:

- [Automated backups](#automated-backups)
- [Traffic load balancer](#traffic-load-balancer)
- [Zero downtime updates](#zero-downtime-updates)
- [Automated database failover](#automated-database-failover)
- [Instance level replication with GitLab Geo](#instance-level-replication-with-gitlab-geo)

As you implement these components, begin with a single server and then do
backups. Only after completing the first server should you proceed to the next.

Also, not implementing extra servers for GitLab doesn't necessarily mean that you'll have
more downtime. Depending on your needs and experience level, single servers can
have more actual perceived uptime for your users.
-->

极狐GitLab 附带了以下组件供您使用，根据复杂度由低到高进行排列：

- [自动备份](#自动备份)
- [流量负载均衡器](#流量负载均衡器)
- [零停机更新](#零停机更新)
- [自动数据库故障转移](#自动数据库故障转移)
- [使用 GitLab Geo 进行实例级复制](#使用 GitLab Geo 进行实例级复制)

从单个服务器开始实施这些组件，然后执行备份。只有在完成第一个服务器后，才能继续下一个。

此外，额外的服务器并不意味着会有更多的停机时间。根据您的需求和经验水平，单个服务器可以为您的用户实际感知的正常运行时间。

### 自动备份

<!--
> - Level of complexity: **Low**
> - Required domain knowledge: PostgreSQL, GitLab configurations, Git

This solution is appropriate for many teams that have the default GitLab installation.
With automatic backups of the GitLab repositories, configuration, and the database,
this can be an optimal solution if you don't have strict requirements.
[Automated backups](../../raketasks/backup_restore.md#configuring-cron-to-make-daily-backups)
is the least complex to setup. This provides a point-in-time recovery of a predetermined schedule.
-->

> - 复杂程度：**低**
> - 所需的领域知识：PostgreSQL、GitLab 配置、Git

该解决方案适用于使用默认极狐GitLab 安装的许多团队。通过极狐GitLab 仓库、配置和数据库的自动备份，如果您没有严格的需求，这可能时最佳解决方案。[自动备份](../../raketasks/backup_restore.md#configuring-cron-to-make-daily-backups)最容易设置，提供了预定计划的时间点恢复功能。

### 流量负载均衡器 **(PREMIUM SELF)**

<!--
> - Level of complexity: **Medium**
> - Required domain knowledge: HAProxy, shared storage, distributed systems

This requires separating out GitLab into multiple application nodes with an added
[load balancer](../load_balancer.md). The load balancer will distribute traffic
across GitLab application nodes. Meanwhile, each application node connects to a
shared file server and database systems on the back end. This way, if one of the
application servers fails, the workflow is not interrupted.
[HAProxy](https://www.haproxy.org/) is recommended as the load balancer.

With this added component you have a number of advantages compared
to the default installation:

- Increase the number of users.
- Enable zero-downtime upgrades.
- Increase availability.

For more details on how to configure a traffic load balancer with GitLab, you can refer
to any of the [available reference architectures](#available-reference-architectures) with more than 1,000 users.
-->

> - 复杂程度：**中**
> - 所需的领域知识：HAProxy, shared storage, distributed systems

需要将极狐GitLab 分成多个应用节点，并添加[负载均衡器](../load_balancer.md)。负载均衡器将在应用节点之间分配流量。同时，每个应用节点都连接到后端的共享文件服务器和数据库系统。这样，如果其中一个应用服务器故障，工作流不会被中断。推荐使用 [HAProxy](https://www.haproxy.org/) 作为负载均衡器。

与默认安装相比，添加该节点有以下优势：

* 增加用户数量。
* 启用零停机升级。
* 提高可用性。

有关如何使用极狐GitLab 配置流量负载均衡器的更多详细信息，您可以参考适用于超过 1000 用户的任何[可用参考架构](#可用参考架构)。

### 零停机更新 **(PREMIUM SELF)**

<!--
> - Level of complexity: **Medium**
> - Required domain knowledge: PostgreSQL, HAProxy, shared storage, distributed systems

GitLab supports [zero-downtime updates](https://docs.gitlab.com/omnibus/update/#zero-downtime-updates).
Single GitLab nodes can be updated with only a [few minutes of downtime](https://docs.gitlab.com/omnibus/update/README.html#single-node-deployment).
To avoid this, we recommend to separate GitLab into several application nodes.
As long as at least one of each component is online and capable of handling the instance's usage load, your team's productivity will not be interrupted during the update.
-->

> - 复杂程度：**中**
> - 所需的领域知识：PostgreSQL, HAProxy, shared storage, distributed systems

极狐GitLab 支持[零停机升级](https://docs.gitlab.cn/omnibus/update/#零停机升级)。只需[几分钟的停机时间](https://docs.gitlab.cn/omnibus/update/README.html#single-node-deployment)即可更新单个节点。为了避免这种情况，我们建议将极狐GitLab 分成多个应用节点。只要组件之一在线并且能够处理实例的使用负载，更新期间，您团队的工作效率不会受到影响。

### 自动数据库故障转移 **(PREMIUM SELF)**

<!--
> - Level of complexity: **High**
> - Required domain knowledge: PgBouncer, Repmgr or Patroni, shared storage, distributed systems

By adding automatic failover for database systems, you can enable higher uptime
with additional database nodes. This extends the default database with
cluster management and failover policies.
[PgBouncer in conjunction with Repmgr or Patroni](../postgresql/replication_and_failover.md)
is recommended.
-->

> - 复杂程度：**高**
> - 所需的领域知识：PgBouncer, Patroni, shared storage, distributed systems

通过为数据库系统添加自动故障转移，您可以使用额外的数据库节点实现更长的正常运行时间。该组件通过集群管理和故障转移策略，扩展了默认数据库。推荐 [PgBouncer 与 Patroni 结合使用](../postgresql/replication_and_failover.md)。

### 使用 GitLab Geo 进行实例级复制 **(PREMIUM SELF)**

<!--
> - Level of complexity: **Very High**
> - Required domain knowledge: Storage replication

[GitLab Geo](../geo/index.md) allows you to replicate your GitLab
instance to other geographical locations as a read-only fully operational instance
that can also be promoted in case of disaster.
-->

> - 复杂程度：**非常高**
> - 所需的领域知识：Storage replication

[GitLab Geo](../geo/index.md) 允许您复制极狐GitLab 实例到其它地理位置，复制的实例是完全可操作性的只读实例，可以在发生灾难时升级。

## 背离建议的参考架构

<!--
As a general rule of thumb, the further away you move from the Reference Architectures,
the harder it will be get support for it. With any deviation, you're introducing
a layer of complexity that will add challenges to finding out where potential
issues might lie.

The reference architectures use the official GitLab Linux packages (Omnibus
GitLab) to install and configure the various components (with one notable exception being the suggested select Cloud Native installation method described below). The components are
installed on separate machines (virtualized or bare metal), with machine hardware
requirements listed in the "Configuration" column and equivalent VM standard sizes listed
in GCP/AWS/Azure columns of each [available reference architecture](#available-reference-architectures).

Running components on Docker (including Compose) with the same specs should be fine, as Docker is well known in terms of support.
However, it is still an additional layer and may still add some support complexities, such as not being able to run `strace` easily in containers.

Other technologies, like [Docker swarm](https://docs.docker.com/engine/swarm/)
are not officially supported, but can be implemented at your own risk. In that
case, GitLab Support will not be able to help you.
-->

根据通常经验，背离参考架构越远，您更难获得技术支持。出现任何偏差都会带来一层复杂性，会给定位潜在问题增加挑战。

参考架构使用官方的 Linux 安装包（Omnibus GitLab）安装和配置各种组件。组件安装在单独的机器上（虚拟机或裸金属），机器硬件需求列在“配置”中，等效的虚拟机标准大小列在每个[可用参考架构](#可用参考架构)的“GCP/AWS/Azure”中。

在 Docker（包括 Compose）上运行具有相同规范的组件应该是适用的，然而还是额外的一层，可能会增加一些支持的复杂性，例如不能在容器中轻松运行 `strace`。

其它技术不受官方支持，例如 [Docker swarm](https://docs.docker.com/engine/swarm/)，您可以自担风险实施。在这种情况下，GitLab 技术支持无法为您提供协助。

<!--
### Configuring select components with Cloud Native Helm

We also provide [Helm charts](https://docs.gitlab.com/charts/) as a Cloud Native installation
method for GitLab. For the reference architectures, select components can be set up in this
way as an alternative if so desired.

For these kind of setups we support using the charts in an [advanced configuration](https://docs.gitlab.com/charts/#advanced-configuration)
where stateful backend components, such as the database or Gitaly, are run externally - either
via Omnibus or reputable third party services. Note that we don't currently support running the
stateful components via Helm _at large scales_.

When designing these environments you should refer to the respective [Reference Architecture](#available-reference-architectures)
above for guidance on sizing. Components run via Helm would be similarly scaled to their Omnibus
specs, only translated into Kubernetes resources.

For example, if you were to set up a 50k installation with the Rails nodes being run in Helm,
then the same amount of resources as given for Omnibus should be given to the Kubernetes
cluster with the Rails nodes broken down into a number of smaller Pods across that cluster.
-->